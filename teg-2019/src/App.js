import React, { Component } from 'react';
import './App.css';
import AppRoot from './app/components';

class App extends Component {
  render() {
    return (
      <div className="App">
        <AppRoot />
      </div>
    );
  }
}

export default App;
