import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Navigation extends Component {
  render() {
    return (
      <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
              <Link to="/" className="nav-link">Inicio</Link>
            </li>
            <li className="nav-item dropdown active">
              <a className="nav-link dropdown-toggle" href="#" id="Dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Configuracion
              </a>
              <div className="dropdown-menu" aria-labelledby="Dropdown">
                <Link to="/Trabajador" className="dropdown-item" >Trabajador</Link>
                <Link to="/Comision" className="dropdown-item" >Comision</Link>
              </div>
            </li>
            <li className="nav-item dropdown active">
              <a className="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Reclamo
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link to="/NuevoReclamo" className="dropdown-item" >Nuevo Reclamo</Link>
                <Link to="/Analisis" className="dropdown-item" >Analisis</Link>
                <Link to="/Control" className="dropdown-item" >Control</Link>
                <Link to="/Soluciones" className="dropdown-item" >Soluciones</Link>
              </div>
            </li>
            <li className="nav-item dropdown active">
              <a className="nav-link dropdown-toggle" href="#" id="Dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Penalizacion
              </a>
              <div className="dropdown-menu" aria-labelledby="Dropdown">
                <Link to="/" className="dropdown-item" >-----</Link>
                <Link to="/" className="dropdown-item" >-----</Link>
              </div>
            </li>
            <li className="nav-item dropdown active">
              <a className="nav-link dropdown-toggle" href="#" id="Dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Estadisticas
              </a>
              <div className="dropdown-menu" aria-labelledby="Dropdown">
                <Link to="/Individuales" className="dropdown-item" >Individuales</Link>
                <Link to="/Grupales" className="dropdown-item" >Grupales</Link>
              </div>
            </li>
            <li className="nav-item dropdown active">
              <a className="nav-link dropdown-toggle" href="#" id="Dropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Administracion
              </a>
              <div className="dropdown-menu" aria-labelledby="Dropdown">
                <Link to="/Usuarios" className="dropdown-item" >Usuarios</Link>
                <Link to="/ManteSistema" className="dropdown-item" >Mantenimiento del Sistema</Link>
                <Link to="/ManteTablas" className="dropdown-item" >Mantenimiento de Tablas</Link>
              </div>
            </li>
          </ul>
        </div>
        </nav>
      </div>
    );
  }
}

export default Navigation;
