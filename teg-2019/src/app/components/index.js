import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Main from './Main.js';
import Claim from './claim/Claim';

class RootRouter extends Component {
  render() {
    return (
      <div>
        <Router>
          <div>
            <Route exact path="/" component={Main} />
            <Route path="/NuevoReclamo" component={Claim} />
          </div>
        </Router>
      </div>
    );
  }
}

export default RootRouter;
